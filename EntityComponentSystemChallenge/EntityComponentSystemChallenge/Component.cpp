#include "Component.h"

Component::Component()
{
}

Component::~Component()
{
}

void Component::Attach(int entityId)
{
	myEntity = entityId;
}

void Component::Detach(int entityId)
{
	if (entityId==myEntity)
	{
		myEntity = -1;
	}
}
