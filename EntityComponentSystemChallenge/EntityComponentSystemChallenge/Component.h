#pragma once

enum Type
{
	Score
};
class Component {

private:
	int myEntity=-1;
public:
	Component();
	~Component();
	int type;
	void Attach(int entityId);
	void Detach(int entityId);
	virtual void Update()=0;
};