#pragma once
#include "Component.h"
#include "Score.h"
#include <vector>

enum EventType
{
	ScoreUpdate
};
class EventManager
{
private:
	std::vector<Component*> allComponents;
public:
	void Update(EventType type)
	{
		for (int i = 0; i < allComponents.size(); i++)
		{
			if (allComponents[i])
			{

			}
			allComponents[i]->Update();
		}
	}
}