#ifndef __Wrapper_h
#define __Wrapper_h

#include "LibSettings.h"
#include "Entity.h"
#include "Component.h"
#include "EventManager.h"

#ifdef __cplusplus
extern "C"
{
#endif
	//Our functions go here
	//void LIB_API LoadDLL();
	Entity* LIB_API CreateEntity();
	Component* LIB_API CreateComponent();
	void LIB_API AttachComponent(Entity* entity, Component* component);
	void LIB_API RemoveComponent(Entity* entity, Component* component);
	void LIB_API SendEvent(int type);
	void LIB_API Update();
	int LIB_API GetScore(Component* component);

#ifdef __cplusplus
};
#endif
#endif