#pragma once
#include "Component.h"
#include "IEntity.h"
#include <vector>

class Entity:IEntity
{
public:
	Entity();
	void AddComponent(Component*);
	void RemoveComponent(Component*);
	~Entity();
private:
	int id;
	std::vector<Component*> myComponents;
};