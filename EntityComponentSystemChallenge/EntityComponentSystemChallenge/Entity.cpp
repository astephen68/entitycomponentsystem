#include "Entity.h"

Entity::Entity()
{
	id = GetId();
}

void Entity::AddComponent(Component * myComponent)
{
	myComponent->Attach(this->id);
	myComponents.push_back(myComponent);
}

void Entity::RemoveComponent(Component *)
{

}

Entity::~Entity()
{

}
