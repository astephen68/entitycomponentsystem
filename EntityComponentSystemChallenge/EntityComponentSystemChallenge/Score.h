#pragma once
#include "Component.h"
class Score :public Component
{
private:
	int value;

public:
	void IncreaseScore(int score)
	{
		value += score;
	}
	
	int GetScore()
	{
		return value;
	}

	void Update() override
	{
		IncreaseScore(1);
	}
};